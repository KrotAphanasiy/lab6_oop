﻿using System.Linq;

namespace Lab6_OOP
{
    class SequenceReader
    {
        public static double[] ReadSequence(string baseString)
        {
            var resultingSequence = baseString.Trim().Split(' ').Select(double.Parse).ToArray();
            return resultingSequence;
        }

        public static int ParseIntValue(string input)
        {
            int.TryParse(string.Join("", input.Where(c => char.IsDigit(c))), out int value);
            return value;
        }
    }
}
