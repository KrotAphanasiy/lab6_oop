﻿using System;


namespace Lab6_OOP
{
    class ExponentialProgression : Progression
    {
        double _secondElement = 0;
        double _infDecrSum = 0;
        bool _infDecrSumCalculated = false;

        public ExponentialProgression(double[] progressionMembs, int elementsAmount) : 
            base(progressionMembs, elementsAmount)
        {
            double prev = _progressionMembs[1] / _progressionMembs[0];

            for (int counter = 2; counter < _elementsAmount; counter++)
            {
                if (!DbsAreSame(_progressionMembs[counter] / _progressionMembs[counter - 1], prev))
                {
                    throw new Exception("Sequence is not progression");
                }
            }

            _secondElement = _progressionMembs[1];
            _feature = _secondElement / _firstElement;
            _featurePassed = true;
        }

        public ExponentialProgression(double firstElement, int elementsAmount, double feature) :
            base(firstElement, elementsAmount, feature)
        {

        }

        public ExponentialProgression(double firstElement, double secondElement, int elementsAmount):
            base(firstElement, elementsAmount)
        {
            _secondElement = secondElement;
        }

        public override double CalculateAbsoluteSum()
        {
            if (Math.Abs(_feature) < 1)
            {
                if (!_infDecrSumCalculated)
                {
                    _infDecrSum = _firstElement / (1 - _feature);
                    _infDecrSumCalculated = true;
                }
                return _infDecrSum;
            }
            else
            {
                throw new Exception("unable to calculate");
            }
        }

        public override double CalculateFeature()
        {
            if (!_featurePassed)
            {
                _feature = _secondElement / _firstElement;
                _featurePassed = true;
            }
            return _feature;
        }

        public override double CalculateNElement(int elNumber)
        {
            CalculateFeature();
            return _firstElement * Math.Pow(_feature, elNumber);
        }

        public override double CalculateSum()
        {
            if (!_sumCalculated)
            {
                if (!_featurePassed)
                {
                    CalculateFeature();
                }
                _sum = _firstElement * (1 - Math.Pow(_feature, _elementsAmount));
                _sum = _sum / (1 - _feature);
                _sumCalculated = true;
            }
            return _sum;
        }
    }
}
