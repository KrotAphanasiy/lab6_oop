﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab6_OOP
{
    enum ProgressionType
    {
        LINEAR_PROGRESSION = 1,
        EXPONENTIAL_PROGRESSION = 2
    }
    class ProgressionUtils
    { 
        private static double[] ReadMembers(int elementsAmount, string baseString)
        {
            var res = SequenceReader.ReadSequence(baseString);
            if (res.Length != elementsAmount)
            {
                throw new Exception("Wrong members amount");
            }

            return res;
        }
        public static Progression CreatePrgoression(ProgressionType progType, int initMethod,
            string baseString, int membersAmount = 0, double feature = 0)
        {
            Progression toReturn = null;
            if (progType == ProgressionType.LINEAR_PROGRESSION)
            {
                if (initMethod == 1)
                {
                    toReturn = new LinearProgression(ReadMembers(membersAmount, baseString), membersAmount);
                }
                else if (initMethod == 2)
                {
                    var arr = SequenceReader.ReadSequence(baseString);
                    try
                    {
                        double first = arr[0];
                        toReturn = new LinearProgression(first, membersAmount, feature);
                    }
                    catch
                    {
                        throw;
                    }
                }
                else if (initMethod == 3)
                {
                    var arr = SequenceReader.ReadSequence(baseString);
                    try
                    {
                        double first = arr[0];
                        double last = arr[1];
                        toReturn = new LinearProgression(first, last, membersAmount);
                    }
                    catch
                    {
                        throw;
                    }
                }
            }
            else if (progType == ProgressionType.EXPONENTIAL_PROGRESSION)
            {
                if (initMethod == 1)
                {
                    toReturn = new ExponentialProgression(ReadMembers(membersAmount, baseString), membersAmount);
                    
                }
                else if (initMethod == 2)
                {
                    var arr = SequenceReader.ReadSequence(baseString);
                    try
                    {
                        double first = arr[0];
                        toReturn = new ExponentialProgression(first, membersAmount, feature);
                    }
                    catch 
                    {
                        throw;
                    }
                }
                else if (initMethod == 4)
                {
                    var arr = SequenceReader.ReadSequence(baseString);
                    try
                    {
                        double first = arr[0];
                        double last = arr[1];
                        toReturn = new ExponentialProgression(first, last, membersAmount);
                    }
                    catch
                    {
                        throw;
                    }
                }
            }
            return toReturn;
        }

        public static void CalculateProgression(ref Progression progression)
        {
            progression.CalculateSum();
            progression.CalculateFeature();
            if (Math.Abs(progression.Feature) < 1)
            {
                try
                {
                    progression.CalculateAbsoluteSum();
                }
                catch 
                {
                    return;
                }
            }

        }

    }
}
