﻿using System;


namespace Lab6_OOP
{
    class LinearProgression : Progression
    {

        double _lastElement = 0;
        public LinearProgression(double[] progressionMembs, int elementsAmount) : 
            base(progressionMembs, elementsAmount) 
        {
            double prev = _progressionMembs[1] - _progressionMembs[0];

            for (int counter = 2; counter < _elementsAmount; counter++)
            {
                if (!DbsAreSame(_progressionMembs[counter] - _progressionMembs[counter - 1], prev))
                {
                    throw new Exception("Sequence is not progression");
                }
            }
            _lastElement = _progressionMembs[_elementsAmount - 1];
        }

        public LinearProgression(double firstElement, int elementsAmount, double feature) :
            base(firstElement, elementsAmount, feature)
        {

        }

        public LinearProgression(double firstElement, double lastElement, int elementsAmount) :
            base(firstElement, elementsAmount)
        {
            _lastElement = lastElement;
        }


        public override double CalculateAbsoluteSum()
        {
            throw new Exception("Can`t calculate for this progression");
        }

        public override double CalculateFeature()
        {
            if (_featurePassed)
            {
                return _feature;
            }
            else if (_progressionMembs != null)
            {
                _feature = _progressionMembs[1] - _progressionMembs[0];
                return _feature;
            }
            else if (_sumCalculated)
            {
                _feature = (_lastElement - _firstElement) / (_elementsAmount - 1);
                return _feature;
            }
            else
            {
                throw new Exception("Can`t calculate Linear feature before sum");
            }
        }

        public override double CalculateNElement(int elNumber)
        {
            CalculateFeature();
            return _firstElement + _feature * elNumber;
        }

        public override double CalculateSum()
        {
            if (!_featurePassed)
            {
                _sum = (_firstElement + _lastElement) / 2;
                _sum = _sum * _elementsAmount;
            }
            else
            {
                _sum = _firstElement * 2 + _feature * (_elementsAmount - 1);
                _sum = _sum / 2;
                _sum = _sum * _elementsAmount;
            }
            _sumCalculated = true;
            return _sum;
        }
    }
}
