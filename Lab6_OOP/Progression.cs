﻿using System;

namespace Lab6_OOP
{
    abstract class Progression 
    {
        protected double[] _progressionMembs = null;
        protected double _firstElement = 0;
        protected double _feature = 0;
        protected double _sum = 0;
        protected bool _sumCalculated = false;
        protected bool _featurePassed = false;
        protected int _elementsAmount = 0;

        public double this[int index]
        {
            get
            {
                if (index < _elementsAmount && _progressionMembs != null)
                {
                    return _progressionMembs[index];
                }
                else
                {
                    return CalculateNElement(index);
                }
            }
        }

        public double Sum
        {
            get
            {
                this.CalculateSum();
                return _sum;
            }
        }

        public double Feature
        {
            get
            {
                this.CalculateFeature();
                return _feature;
            }
        }

        public bool FeatureStatus
        {
            get
            {
                return _featurePassed;
            }
        }

        public int ElementsAmount
        {
            get
            {
                return _elementsAmount;
            }
        }

        protected bool DbsAreSame(double a, double b)
        {
            return Math.Abs(a - b) < 1e-8;
        }

        public Progression()
        {

        }

        public Progression(double[] progressionMembs, int elementsAmount)
        {
            _progressionMembs = progressionMembs;
            _elementsAmount = elementsAmount;

            if (_progressionMembs != null && _elementsAmount > 1)
            {
                _firstElement = _progressionMembs[0];
            }
            else
            {
                throw new Exception("Progression is empty or too short");
            }
        }


        public Progression(double firstElement, int elementsAmount = 0, double feature = 0)
        {
            _firstElement = firstElement;
            _elementsAmount = elementsAmount;
            _feature = feature;

            if (_feature != 0)
            {
                _featurePassed = true;
            }

        }

        abstract public double CalculateSum();
        abstract public double CalculateNElement(int elNumber);
        abstract public double CalculateFeature();
        abstract public double CalculateAbsoluteSum();
    }
}
