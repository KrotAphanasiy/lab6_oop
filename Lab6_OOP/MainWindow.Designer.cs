﻿namespace Lab6_OOP
{
    partial class MainWindow
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.calculateButton = new System.Windows.Forms.Button();
            this.groupBoxProgType = new System.Windows.Forms.GroupBox();
            this.radioButtonExponential = new System.Windows.Forms.RadioButton();
            this.radioButtonLinear = new System.Windows.Forms.RadioButton();
            this.groupBoxSubmission = new System.Windows.Forms.GroupBox();
            this.textBoxFeatureInfo = new System.Windows.Forms.TextBox();
            this.textBoxAmountInfo = new System.Windows.Forms.TextBox();
            this.textBoxFeatureSubmitted = new System.Windows.Forms.TextBox();
            this.textBoxAmountSubmitted = new System.Windows.Forms.TextBox();
            this.textBoxInitStr = new System.Windows.Forms.TextBox();
            this.textBoxMethodInfo = new System.Windows.Forms.TextBox();
            this.textBoxTypeName = new System.Windows.Forms.TextBox();
            this.textBoxInitStrSubmitted = new System.Windows.Forms.TextBox();
            this.textBoxInitMethodSubmitted = new System.Windows.Forms.TextBox();
            this.textBoxTypeSubmitted = new System.Windows.Forms.TextBox();
            this.groupBoxInitMethod = new System.Windows.Forms.GroupBox();
            this.radioButtonInit4 = new System.Windows.Forms.RadioButton();
            this.radioButtonInit3 = new System.Windows.Forms.RadioButton();
            this.radioButtonInit2 = new System.Windows.Forms.RadioButton();
            this.radioButtonInit1 = new System.Windows.Forms.RadioButton();
            this.initData = new System.Windows.Forms.TextBox();
            this.buttonSubmit = new System.Windows.Forms.Button();
            this.groupBoxResults = new System.Windows.Forms.GroupBox();
            this.textBoxResAbsSumVal = new System.Windows.Forms.TextBox();
            this.textBoxResFeatureVal = new System.Windows.Forms.TextBox();
            this.textBoxResSumVal = new System.Windows.Forms.TextBox();
            this.textBoxResMembersVal = new System.Windows.Forms.TextBox();
            this.textBoxResAbsSum = new System.Windows.Forms.TextBox();
            this.textBoxResFeature = new System.Windows.Forms.TextBox();
            this.textBoxResSum = new System.Windows.Forms.TextBox();
            this.textBoxResMembers = new System.Windows.Forms.TextBox();
            this.textBoxMembersAmount = new System.Windows.Forms.TextBox();
            this.textBoxFeature = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.groupBoxProgType.SuspendLayout();
            this.groupBoxSubmission.SuspendLayout();
            this.groupBoxInitMethod.SuspendLayout();
            this.groupBoxResults.SuspendLayout();
            this.SuspendLayout();
            // 
            // calculateButton
            // 
            this.calculateButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calculateButton.Location = new System.Drawing.Point(410, 61);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Size = new System.Drawing.Size(172, 51);
            this.calculateButton.TabIndex = 0;
            this.calculateButton.Text = "Calculate";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.CalculateButtonClick);
            // 
            // groupBoxProgType
            // 
            this.groupBoxProgType.Controls.Add(this.radioButtonExponential);
            this.groupBoxProgType.Controls.Add(this.radioButtonLinear);
            this.groupBoxProgType.Location = new System.Drawing.Point(12, 12);
            this.groupBoxProgType.Name = "groupBoxProgType";
            this.groupBoxProgType.Size = new System.Drawing.Size(172, 79);
            this.groupBoxProgType.TabIndex = 1;
            this.groupBoxProgType.TabStop = false;
            this.groupBoxProgType.Text = "Choose progression type";
            // 
            // radioButtonExponential
            // 
            this.radioButtonExponential.AutoSize = true;
            this.radioButtonExponential.Location = new System.Drawing.Point(7, 49);
            this.radioButtonExponential.Name = "radioButtonExponential";
            this.radioButtonExponential.Size = new System.Drawing.Size(87, 19);
            this.radioButtonExponential.TabIndex = 1;
            this.radioButtonExponential.TabStop = true;
            this.radioButtonExponential.Text = "Exponential";
            this.radioButtonExponential.UseVisualStyleBackColor = true;
            this.radioButtonExponential.CheckedChanged += new System.EventHandler(this.RadioButtonExponential_CheckedChanged);
            // 
            // radioButtonLinear
            // 
            this.radioButtonLinear.AutoSize = true;
            this.radioButtonLinear.Location = new System.Drawing.Point(7, 23);
            this.radioButtonLinear.Name = "radioButtonLinear";
            this.radioButtonLinear.Size = new System.Drawing.Size(57, 19);
            this.radioButtonLinear.TabIndex = 0;
            this.radioButtonLinear.TabStop = true;
            this.radioButtonLinear.Text = "Linear";
            this.radioButtonLinear.UseVisualStyleBackColor = true;
            this.radioButtonLinear.CheckedChanged += new System.EventHandler(this.RadioButtonLinear_CheckedChanged);
            // 
            // groupBoxSubmission
            // 
            this.groupBoxSubmission.Controls.Add(this.textBoxFeatureInfo);
            this.groupBoxSubmission.Controls.Add(this.textBoxAmountInfo);
            this.groupBoxSubmission.Controls.Add(this.textBoxFeatureSubmitted);
            this.groupBoxSubmission.Controls.Add(this.textBoxAmountSubmitted);
            this.groupBoxSubmission.Controls.Add(this.textBoxInitStr);
            this.groupBoxSubmission.Controls.Add(this.textBoxMethodInfo);
            this.groupBoxSubmission.Controls.Add(this.textBoxTypeName);
            this.groupBoxSubmission.Controls.Add(this.textBoxInitStrSubmitted);
            this.groupBoxSubmission.Controls.Add(this.textBoxInitMethodSubmitted);
            this.groupBoxSubmission.Controls.Add(this.textBoxTypeSubmitted);
            this.groupBoxSubmission.Location = new System.Drawing.Point(214, 12);
            this.groupBoxSubmission.Name = "groupBoxSubmission";
            this.groupBoxSubmission.Size = new System.Drawing.Size(172, 179);
            this.groupBoxSubmission.TabIndex = 2;
            this.groupBoxSubmission.TabStop = false;
            this.groupBoxSubmission.Text = "Submited to calculate";
            // 
            // textBoxFeatureInfo
            // 
            this.textBoxFeatureInfo.BackColor = System.Drawing.SystemColors.Menu;
            this.textBoxFeatureInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxFeatureInfo.Enabled = false;
            this.textBoxFeatureInfo.Location = new System.Drawing.Point(6, 95);
            this.textBoxFeatureInfo.Name = "textBoxFeatureInfo";
            this.textBoxFeatureInfo.Size = new System.Drawing.Size(62, 16);
            this.textBoxFeatureInfo.TabIndex = 2;
            this.textBoxFeatureInfo.Text = "Feature";
            // 
            // textBoxAmountInfo
            // 
            this.textBoxAmountInfo.BackColor = System.Drawing.SystemColors.Menu;
            this.textBoxAmountInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxAmountInfo.Enabled = false;
            this.textBoxAmountInfo.Location = new System.Drawing.Point(6, 73);
            this.textBoxAmountInfo.Name = "textBoxAmountInfo";
            this.textBoxAmountInfo.Size = new System.Drawing.Size(62, 16);
            this.textBoxAmountInfo.TabIndex = 2;
            this.textBoxAmountInfo.Text = "Amount";
            // 
            // textBoxFeatureSubmitted
            // 
            this.textBoxFeatureSubmitted.BackColor = System.Drawing.SystemColors.Menu;
            this.textBoxFeatureSubmitted.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxFeatureSubmitted.Enabled = false;
            this.textBoxFeatureSubmitted.Location = new System.Drawing.Point(81, 95);
            this.textBoxFeatureSubmitted.Name = "textBoxFeatureSubmitted";
            this.textBoxFeatureSubmitted.Size = new System.Drawing.Size(74, 16);
            this.textBoxFeatureSubmitted.TabIndex = 0;
            // 
            // textBoxAmountSubmitted
            // 
            this.textBoxAmountSubmitted.BackColor = System.Drawing.SystemColors.Menu;
            this.textBoxAmountSubmitted.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxAmountSubmitted.Enabled = false;
            this.textBoxAmountSubmitted.Location = new System.Drawing.Point(81, 73);
            this.textBoxAmountSubmitted.Name = "textBoxAmountSubmitted";
            this.textBoxAmountSubmitted.Size = new System.Drawing.Size(74, 16);
            this.textBoxAmountSubmitted.TabIndex = 0;
            // 
            // textBoxInitStr
            // 
            this.textBoxInitStr.BackColor = System.Drawing.SystemColors.Menu;
            this.textBoxInitStr.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxInitStr.Enabled = false;
            this.textBoxInitStr.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBoxInitStr.Location = new System.Drawing.Point(36, 128);
            this.textBoxInitStr.Name = "textBoxInitStr";
            this.textBoxInitStr.Size = new System.Drawing.Size(100, 16);
            this.textBoxInitStr.TabIndex = 3;
            this.textBoxInitStr.Text = "Init string";
            this.textBoxInitStr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxMethodInfo
            // 
            this.textBoxMethodInfo.BackColor = System.Drawing.SystemColors.Menu;
            this.textBoxMethodInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxMethodInfo.Enabled = false;
            this.textBoxMethodInfo.Location = new System.Drawing.Point(6, 51);
            this.textBoxMethodInfo.Name = "textBoxMethodInfo";
            this.textBoxMethodInfo.Size = new System.Drawing.Size(62, 16);
            this.textBoxMethodInfo.TabIndex = 2;
            this.textBoxMethodInfo.Text = "Init method";
            // 
            // textBoxTypeName
            // 
            this.textBoxTypeName.BackColor = System.Drawing.SystemColors.Menu;
            this.textBoxTypeName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxTypeName.Enabled = false;
            this.textBoxTypeName.Location = new System.Drawing.Point(6, 25);
            this.textBoxTypeName.Name = "textBoxTypeName";
            this.textBoxTypeName.Size = new System.Drawing.Size(68, 16);
            this.textBoxTypeName.TabIndex = 1;
            this.textBoxTypeName.Text = "Type is ";
            // 
            // textBoxInitStrSubmitted
            // 
            this.textBoxInitStrSubmitted.BackColor = System.Drawing.SystemColors.Menu;
            this.textBoxInitStrSubmitted.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxInitStrSubmitted.Enabled = false;
            this.textBoxInitStrSubmitted.Location = new System.Drawing.Point(6, 150);
            this.textBoxInitStrSubmitted.Name = "textBoxInitStrSubmitted";
            this.textBoxInitStrSubmitted.Size = new System.Drawing.Size(160, 23);
            this.textBoxInitStrSubmitted.TabIndex = 0;
            this.textBoxInitStrSubmitted.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxInitMethodSubmitted
            // 
            this.textBoxInitMethodSubmitted.BackColor = System.Drawing.SystemColors.Menu;
            this.textBoxInitMethodSubmitted.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxInitMethodSubmitted.Enabled = false;
            this.textBoxInitMethodSubmitted.Location = new System.Drawing.Point(81, 51);
            this.textBoxInitMethodSubmitted.Name = "textBoxInitMethodSubmitted";
            this.textBoxInitMethodSubmitted.Size = new System.Drawing.Size(74, 16);
            this.textBoxInitMethodSubmitted.TabIndex = 0;
            // 
            // textBoxTypeSubmitted
            // 
            this.textBoxTypeSubmitted.BackColor = System.Drawing.SystemColors.Menu;
            this.textBoxTypeSubmitted.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxTypeSubmitted.Enabled = false;
            this.textBoxTypeSubmitted.Location = new System.Drawing.Point(81, 25);
            this.textBoxTypeSubmitted.Name = "textBoxTypeSubmitted";
            this.textBoxTypeSubmitted.Size = new System.Drawing.Size(74, 16);
            this.textBoxTypeSubmitted.TabIndex = 0;
            // 
            // groupBoxInitMethod
            // 
            this.groupBoxInitMethod.Controls.Add(this.radioButtonInit4);
            this.groupBoxInitMethod.Controls.Add(this.radioButtonInit3);
            this.groupBoxInitMethod.Controls.Add(this.radioButtonInit2);
            this.groupBoxInitMethod.Controls.Add(this.radioButtonInit1);
            this.groupBoxInitMethod.Location = new System.Drawing.Point(12, 97);
            this.groupBoxInitMethod.Name = "groupBoxInitMethod";
            this.groupBoxInitMethod.Size = new System.Drawing.Size(172, 126);
            this.groupBoxInitMethod.TabIndex = 3;
            this.groupBoxInitMethod.TabStop = false;
            this.groupBoxInitMethod.Text = "Choose init method";
            // 
            // radioButtonInit4
            // 
            this.radioButtonInit4.AutoSize = true;
            this.radioButtonInit4.Enabled = false;
            this.radioButtonInit4.Location = new System.Drawing.Point(7, 101);
            this.radioButtonInit4.Name = "radioButtonInit4";
            this.radioButtonInit4.Size = new System.Drawing.Size(124, 19);
            this.radioButtonInit4.TabIndex = 3;
            this.radioButtonInit4.Text = "1-st, 2-nd, amount";
            this.radioButtonInit4.UseVisualStyleBackColor = true;
            this.radioButtonInit4.CheckedChanged += new System.EventHandler(this.RadioButtonInit4_CheckedChanged);
            // 
            // radioButtonInit3
            // 
            this.radioButtonInit3.AutoSize = true;
            this.radioButtonInit3.Enabled = false;
            this.radioButtonInit3.Location = new System.Drawing.Point(7, 75);
            this.radioButtonInit3.Name = "radioButtonInit3";
            this.radioButtonInit3.Size = new System.Drawing.Size(137, 19);
            this.radioButtonInit3.TabIndex = 2;
            this.radioButtonInit3.Text = "1-st, last and amount";
            this.radioButtonInit3.UseVisualStyleBackColor = true;
            this.radioButtonInit3.CheckedChanged += new System.EventHandler(this.RadioButtonInit3_CheckedChanged);
            // 
            // radioButtonInit2
            // 
            this.radioButtonInit2.AutoSize = true;
            this.radioButtonInit2.Enabled = false;
            this.radioButtonInit2.Location = new System.Drawing.Point(7, 49);
            this.radioButtonInit2.Name = "radioButtonInit2";
            this.radioButtonInit2.Size = new System.Drawing.Size(142, 19);
            this.radioButtonInit2.TabIndex = 1;
            this.radioButtonInit2.Text = "1-st mem, am, feature";
            this.radioButtonInit2.UseVisualStyleBackColor = true;
            this.radioButtonInit2.CheckedChanged += new System.EventHandler(this.RadioButtonInit2_CheckedChanged);
            // 
            // radioButtonInit1
            // 
            this.radioButtonInit1.AutoSize = true;
            this.radioButtonInit1.Enabled = false;
            this.radioButtonInit1.Location = new System.Drawing.Point(7, 23);
            this.radioButtonInit1.Name = "radioButtonInit1";
            this.radioButtonInit1.Size = new System.Drawing.Size(145, 19);
            this.radioButtonInit1.TabIndex = 0;
            this.radioButtonInit1.Text = "Amount and members";
            this.radioButtonInit1.UseVisualStyleBackColor = true;
            this.radioButtonInit1.CheckedChanged += new System.EventHandler(this.RadioButtonInit1_CheckedChanged);
            // 
            // initData
            // 
            this.initData.Enabled = false;
            this.initData.Location = new System.Drawing.Point(12, 270);
            this.initData.Name = "initData";
            this.initData.Size = new System.Drawing.Size(172, 23);
            this.initData.TabIndex = 4;
            this.initData.TextChanged += new System.EventHandler(this.InitData_TextChanged);
            // 
            // buttonSubmit
            // 
            this.buttonSubmit.Enabled = false;
            this.buttonSubmit.Location = new System.Drawing.Point(109, 299);
            this.buttonSubmit.Name = "buttonSubmit";
            this.buttonSubmit.Size = new System.Drawing.Size(75, 23);
            this.buttonSubmit.TabIndex = 5;
            this.buttonSubmit.Text = "Submit";
            this.buttonSubmit.UseVisualStyleBackColor = true;
            this.buttonSubmit.Click += new System.EventHandler(this.SubmitButtonClick);
            // 
            // groupBoxResults
            // 
            this.groupBoxResults.Controls.Add(this.textBoxResAbsSumVal);
            this.groupBoxResults.Controls.Add(this.textBoxResFeatureVal);
            this.groupBoxResults.Controls.Add(this.textBoxResSumVal);
            this.groupBoxResults.Controls.Add(this.textBoxResMembersVal);
            this.groupBoxResults.Controls.Add(this.textBoxResAbsSum);
            this.groupBoxResults.Controls.Add(this.textBoxResFeature);
            this.groupBoxResults.Controls.Add(this.textBoxResSum);
            this.groupBoxResults.Controls.Add(this.textBoxResMembers);
            this.groupBoxResults.Location = new System.Drawing.Point(214, 201);
            this.groupBoxResults.Name = "groupBoxResults";
            this.groupBoxResults.Size = new System.Drawing.Size(396, 121);
            this.groupBoxResults.TabIndex = 6;
            this.groupBoxResults.TabStop = false;
            this.groupBoxResults.Text = "Results";
            // 
            // textBoxResAbsSumVal
            // 
            this.textBoxResAbsSumVal.BackColor = System.Drawing.SystemColors.Menu;
            this.textBoxResAbsSumVal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxResAbsSumVal.CausesValidation = false;
            this.textBoxResAbsSumVal.Enabled = false;
            this.textBoxResAbsSumVal.Location = new System.Drawing.Point(112, 79);
            this.textBoxResAbsSumVal.Name = "textBoxResAbsSumVal";
            this.textBoxResAbsSumVal.Size = new System.Drawing.Size(278, 16);
            this.textBoxResAbsSumVal.TabIndex = 1;
            // 
            // textBoxResFeatureVal
            // 
            this.textBoxResFeatureVal.BackColor = System.Drawing.SystemColors.Menu;
            this.textBoxResFeatureVal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxResFeatureVal.CausesValidation = false;
            this.textBoxResFeatureVal.Enabled = false;
            this.textBoxResFeatureVal.Location = new System.Drawing.Point(112, 57);
            this.textBoxResFeatureVal.Name = "textBoxResFeatureVal";
            this.textBoxResFeatureVal.Size = new System.Drawing.Size(278, 16);
            this.textBoxResFeatureVal.TabIndex = 1;
            // 
            // textBoxResSumVal
            // 
            this.textBoxResSumVal.BackColor = System.Drawing.SystemColors.Menu;
            this.textBoxResSumVal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxResSumVal.CausesValidation = false;
            this.textBoxResSumVal.Enabled = false;
            this.textBoxResSumVal.Location = new System.Drawing.Point(112, 37);
            this.textBoxResSumVal.Name = "textBoxResSumVal";
            this.textBoxResSumVal.Size = new System.Drawing.Size(278, 16);
            this.textBoxResSumVal.TabIndex = 1;
            // 
            // textBoxResMembersVal
            // 
            this.textBoxResMembersVal.BackColor = System.Drawing.SystemColors.Menu;
            this.textBoxResMembersVal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxResMembersVal.CausesValidation = false;
            this.textBoxResMembersVal.Enabled = false;
            this.textBoxResMembersVal.Location = new System.Drawing.Point(112, 19);
            this.textBoxResMembersVal.Name = "textBoxResMembersVal";
            this.textBoxResMembersVal.Size = new System.Drawing.Size(278, 16);
            this.textBoxResMembersVal.TabIndex = 1;
            // 
            // textBoxResAbsSum
            // 
            this.textBoxResAbsSum.BackColor = System.Drawing.SystemColors.Menu;
            this.textBoxResAbsSum.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxResAbsSum.Enabled = false;
            this.textBoxResAbsSum.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxResAbsSum.Location = new System.Drawing.Point(6, 82);
            this.textBoxResAbsSum.Name = "textBoxResAbsSum";
            this.textBoxResAbsSum.Size = new System.Drawing.Size(100, 13);
            this.textBoxResAbsSum.TabIndex = 0;
            // 
            // textBoxResFeature
            // 
            this.textBoxResFeature.BackColor = System.Drawing.SystemColors.Menu;
            this.textBoxResFeature.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxResFeature.Enabled = false;
            this.textBoxResFeature.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxResFeature.Location = new System.Drawing.Point(6, 60);
            this.textBoxResFeature.Name = "textBoxResFeature";
            this.textBoxResFeature.Size = new System.Drawing.Size(100, 13);
            this.textBoxResFeature.TabIndex = 0;
            this.textBoxResFeature.Text = "FEATURE :";
            // 
            // textBoxResSum
            // 
            this.textBoxResSum.BackColor = System.Drawing.SystemColors.Menu;
            this.textBoxResSum.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxResSum.Enabled = false;
            this.textBoxResSum.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxResSum.Location = new System.Drawing.Point(6, 40);
            this.textBoxResSum.Name = "textBoxResSum";
            this.textBoxResSum.Size = new System.Drawing.Size(100, 13);
            this.textBoxResSum.TabIndex = 0;
            this.textBoxResSum.Text = "SUM :";
            // 
            // textBoxResMembers
            // 
            this.textBoxResMembers.BackColor = System.Drawing.SystemColors.Menu;
            this.textBoxResMembers.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxResMembers.Enabled = false;
            this.textBoxResMembers.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxResMembers.Location = new System.Drawing.Point(6, 22);
            this.textBoxResMembers.Name = "textBoxResMembers";
            this.textBoxResMembers.Size = new System.Drawing.Size(100, 13);
            this.textBoxResMembers.TabIndex = 0;
            this.textBoxResMembers.Text = "MEMBERS : ";
            // 
            // textBoxMembersAmount
            // 
            this.textBoxMembersAmount.Enabled = false;
            this.textBoxMembersAmount.Location = new System.Drawing.Point(13, 241);
            this.textBoxMembersAmount.Name = "textBoxMembersAmount";
            this.textBoxMembersAmount.Size = new System.Drawing.Size(45, 23);
            this.textBoxMembersAmount.TabIndex = 7;
            // 
            // textBoxFeature
            // 
            this.textBoxFeature.Enabled = false;
            this.textBoxFeature.Location = new System.Drawing.Point(64, 241);
            this.textBoxFeature.Name = "textBoxFeature";
            this.textBoxFeature.Size = new System.Drawing.Size(42, 23);
            this.textBoxFeature.TabIndex = 8;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Enabled = false;
            this.textBox1.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBox1.Location = new System.Drawing.Point(13, 226);
            this.textBox1.Margin = new System.Windows.Forms.Padding(0);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(40, 13);
            this.textBox1.TabIndex = 9;
            this.textBox1.Text = "Amount";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Enabled = false;
            this.textBox2.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBox2.Location = new System.Drawing.Point(64, 226);
            this.textBox2.Margin = new System.Windows.Forms.Padding(0);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(40, 13);
            this.textBox2.TabIndex = 9;
            this.textBox2.Text = "Feature";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.ClientSize = new System.Drawing.Size(622, 331);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.textBoxFeature);
            this.Controls.Add(this.textBoxMembersAmount);
            this.Controls.Add(this.groupBoxResults);
            this.Controls.Add(this.buttonSubmit);
            this.Controls.Add(this.initData);
            this.Controls.Add(this.groupBoxInitMethod);
            this.Controls.Add(this.groupBoxSubmission);
            this.Controls.Add(this.groupBoxProgType);
            this.Controls.Add(this.calculateButton);
            this.Name = "MainWindow";
            this.Text = "Progression Calculator";
            this.Load += new System.EventHandler(this.LoadMainWindow);
            this.groupBoxProgType.ResumeLayout(false);
            this.groupBoxProgType.PerformLayout();
            this.groupBoxSubmission.ResumeLayout(false);
            this.groupBoxSubmission.PerformLayout();
            this.groupBoxInitMethod.ResumeLayout(false);
            this.groupBoxInitMethod.PerformLayout();
            this.groupBoxResults.ResumeLayout(false);
            this.groupBoxResults.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.GroupBox groupBoxProgType;
        private System.Windows.Forms.RadioButton radioButtonExponential;
        private System.Windows.Forms.RadioButton radioButtonLinear;
        private System.Windows.Forms.GroupBox groupBoxSubmission;
        private System.Windows.Forms.GroupBox groupBoxInitMethod;
        private System.Windows.Forms.TextBox initData;
        private System.Windows.Forms.Button buttonSubmit;
        private System.Windows.Forms.GroupBox groupBoxResults;
        private System.Windows.Forms.RadioButton radioButtonInit3;
        private System.Windows.Forms.RadioButton radioButtonInit2;
        private System.Windows.Forms.RadioButton radioButtonInit1;
        private System.Windows.Forms.RadioButton radioButtonInit4;
        private System.Windows.Forms.TextBox textBoxInitStrSubmitted;
        private System.Windows.Forms.TextBox textBoxInitMethodSubmitted;
        private System.Windows.Forms.TextBox textBoxTypeSubmitted;
        private System.Windows.Forms.TextBox textBoxTypeName;
        private System.Windows.Forms.TextBox textBoxMethodInfo;
        private System.Windows.Forms.TextBox textBoxInitStr;
        private System.Windows.Forms.TextBox textBoxMembersAmount;
        private System.Windows.Forms.TextBox textBoxFeature;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBoxFeatureSubmitted;
        private System.Windows.Forms.TextBox textBoxAmountSubmitted;
        private System.Windows.Forms.TextBox textBoxFeatureInfo;
        private System.Windows.Forms.TextBox textBoxAmountInfo;
        private System.Windows.Forms.TextBox textBoxResAbsSum;
        private System.Windows.Forms.TextBox textBoxResFeature;
        private System.Windows.Forms.TextBox textBoxResSum;
        private System.Windows.Forms.TextBox textBoxResMembers;
        private System.Windows.Forms.TextBox textBoxResAbsSumVal;
        private System.Windows.Forms.TextBox textBoxResFeatureVal;
        private System.Windows.Forms.TextBox textBoxResSumVal;
        private System.Windows.Forms.TextBox textBoxResMembersVal;
    }
}

