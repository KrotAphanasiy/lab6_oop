﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab6_OOP
{
    public partial class MainWindow : Form
    {
        private Progression _processedProgression = null;
        private ProgressionType _progType = 0;
        private int _initMethod = 0;
        private ProgressionType _progTypeSubmitted = 0;
        private int _initMethodSubmitted = 0;
        private string _initStringSubmitted = "";
        private int _membersAmountSubmitted = 0;
        private double _featureSubmitted = 0;
        public MainWindow()
        {
            InitializeComponent();
        }
        

        public void LoadMainWindow(object sender, EventArgs e)
        {
            MessageBox.Show("Welcome to progression calculator!");
        }

        private void CalculateButtonClick(object sender, EventArgs e)
        {
            if (_processedProgression == null)
            {
                MessageBox.Show("You have to create progression first!");
            }
            else{
                ProgressionUtils.CalculateProgression(ref _processedProgression);
                ShowResults();
            }
        }

        private void RadioButtonLinear_CheckedChanged(object sender, EventArgs e)
        {
            _progType = ProgressionType.LINEAR_PROGRESSION;
            radioButtonInit1.Enabled = true;
            radioButtonInit2.Enabled = true;
            radioButtonInit3.Enabled = true;
            radioButtonInit4.Enabled = false;
            if (radioButtonInit4.Checked)
            {
                radioButtonInit4.Checked = false;
            }
        }

        private void RadioButtonExponential_CheckedChanged(object sender, EventArgs e)
        {
            _progType = ProgressionType.EXPONENTIAL_PROGRESSION;
            radioButtonInit1.Enabled = true;
            radioButtonInit2.Enabled = true;
            radioButtonInit3.Enabled = false;
            if (radioButtonInit3.Checked)
            {
                radioButtonInit3.Checked = false;
            }
            radioButtonInit4.Enabled = true;
        }

        private void SubmitButtonClick(object sender, EventArgs e)
        {
            _initStringSubmitted = initData.Text.ToString();
            _progTypeSubmitted = _progType;
            _initMethodSubmitted = _initMethod;
            if (!textBoxMembersAmount.Text.Equals("")) { 
                _membersAmountSubmitted = Convert.ToInt32(textBoxMembersAmount.Text);
            }
            else
            {
                MessageBox.Show("Amount not set!");
                return;
            }

            if(!textBoxFeature.Text.Equals("") && (_initMethodSubmitted == 2))
            {
                try
                {
                    _featureSubmitted = Convert.ToDouble(textBoxFeature.Text);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                    return;
                }
            }
            else if(_initMethodSubmitted == 2)
            {
                MessageBox.Show("Feature not set");
                return;
            }
    
            if(_progTypeSubmitted == ProgressionType.LINEAR_PROGRESSION)
            {
                textBoxTypeSubmitted.Text = radioButtonLinear.Text;
            }
            else
            {
                textBoxTypeSubmitted.Text = radioButtonExponential.Text;
            }

            textBoxInitMethodSubmitted.Text = _initMethodSubmitted.ToString();

            textBoxInitStrSubmitted.Text = initData.Text;

            textBoxAmountSubmitted.Text = _membersAmountSubmitted.ToString();

            if (_initMethod == 2)
            {
                textBoxFeatureSubmitted.Text = _featureSubmitted.ToString();
            }
            else
            {
                textBoxFeatureSubmitted.Text = "";
            }
            try
            {
                _processedProgression = ProgressionUtils.CreatePrgoression(_progTypeSubmitted, _initMethodSubmitted, _initStringSubmitted,
                    _membersAmountSubmitted, _featureSubmitted);
            }
            catch(Exception exc)
            {
                MessageBox.Show(exc.Message);
                return;
            }
        }

        private void RadioButtonInit2_CheckedChanged(object sender, EventArgs e)
        {
            _initMethod = 2;
            textBoxMembersAmount.Enabled = true;
            textBoxFeature.Enabled = true;
            EnableProgTextBoxes();
        }

        private void RadioButtonInit1_CheckedChanged(object sender, EventArgs e)
        {
            _initMethod = 1;
            textBoxMembersAmount.Enabled = true;
            textBoxFeature.Enabled = false;
            EnableProgTextBoxes();
        }

        private void RadioButtonInit3_CheckedChanged(object sender, EventArgs e)
        {
            _initMethod = 3;
            textBoxMembersAmount.Enabled = true;
            textBoxFeature.Enabled = false;
            EnableProgTextBoxes();
        }

        private void RadioButtonInit4_CheckedChanged(object sender, EventArgs e)
        {
            _initMethod = 4;
            textBoxMembersAmount.Enabled = true;
            textBoxFeature.Enabled = false;
            EnableProgTextBoxes();
        }

        private void InitData_TextChanged(object sender, EventArgs e)
        {
            buttonSubmit.Enabled = true;
        }


        private void ShowResults()
        {
            StringBuilder membsString = new StringBuilder();
            for (int i = 0; i < _membersAmountSubmitted; i++)
            {
                membsString.Append(_processedProgression[i]);
                membsString.Append(" ");
            }
            textBoxResMembersVal.Text = membsString.ToString();
            textBoxResSumVal.Text = _processedProgression.Sum.ToString();
            textBoxResFeatureVal.Text = _processedProgression.Feature.ToString();
            if (_progTypeSubmitted == ProgressionType.EXPONENTIAL_PROGRESSION 
                && Math.Abs(_processedProgression.Feature) < 1)
            {
                textBoxResAbsSum.Text = "ABSOLUTE SUM :";
                textBoxResAbsSumVal.Text = _processedProgression.CalculateAbsoluteSum().ToString();
            }
            else
            {
                textBoxResAbsSum.Text = "";
                textBoxResAbsSumVal.Text = "";
            }
        }

        private void EnableProgTextBoxes()
        {
            textBoxMembersAmount.Enabled = true;
            initData.Enabled = true;
        }

      
    }
}
